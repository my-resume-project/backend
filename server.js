const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

require('dotenv').config()

const db = require("./app/models");

db.sequelize.sync().then(() => {
  console.log("Re-sync db.");
}).catch((err) => {
  console.log(err)
});

/* var corsOptions = {
  origin: "http://localhost:5173"
}; */

app.use(cors());

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Hello world, it's my backend resume app" });
});

require('./app/routes/auth.routes')(app);
require('./app/routes/content.routes')(app);
require('./app/routes/feedback.routes')(app);

// set port, listen for requests
const PORT = 10000

app.listen(PORT, () => {
    console.log("Server is running....")
})