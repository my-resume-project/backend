exports.publicAccess = (req, res) => {
  res.status(200).send("Public data access");
};

exports.privateAccess = (req, res) => {
  res.status(200).send(
    {
      gitlabLink: 'https://gitlab.com/my-resume-project',
      youtubeLink: 'https://www.youtube.com/embed/XuI7wCwGbLs'
    }
  );
};