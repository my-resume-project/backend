const db = require("../models");
const config = require("../config/auth.config");
const User = db.user;
const Feedback = db.feedback;

exports.createFeedback = (req, res) => {
  return Feedback.create({
    email: req.body.email,
    subject: req.body.subject,
    message: req.body.message,
    userId: req.body.userId
  })
    .then((feedback) => {
      console.log(">> Created feedback: " + JSON.stringify(feedback, null, 4));
      res.send({ message: "Спасибо за оставленную обратную связь. Мы с вами свяжемся." });
    })
    .catch((err) => {
      console.log(">> Error while creating feedback: ", err);
    });
};

exports.findUserById = (commentId) => {
  return User.findByPk(commentId, { include: ["feedback"] })
    .then((user) => {
      return user;
    })
    .catch((err) => {
      console.log(">> Error while finding user: ", err);
    });
};

exports.findFeedbackById = (id) => {
  return Feedback.findByPk(id, { include: ["user"] })
    .then((feedback) => {
      return feedback;
    })
    .catch((err) => {
      console.log(">> Error while finding feedback: ", err);
    });
};

exports.findAll = () => {
  return User.findAll({
    include: ["feedback"],
  }).then((users) => {
    return users;
  });
};