const { authJwt } = require("../middleware");
const controller = require("../controllers/content.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get(
    "/api/content/getPrivateData",
    [
      authJwt.verifyToken
    ],
    controller.privateAccess
  );

  app.get("/api/content/getData", controller.publicAccess);
};