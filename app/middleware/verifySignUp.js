const db = require("../models");
const User = db.user;

checkDuplicateUsernameOrEmail = (req, res, next) => {
  if (req.body.login && req.body.password && req.body.name) {
    
    const loginregex = /^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$/;
    const passregex = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
    const nameregex = /^[а-яА-ЯёЁa-zA-Z]+$/;

    if(!loginregex.test(req.body.login)) {
      res.status(452).send({
        message: "Минимальная длина логина: 3 символа. Ваш логин может содержать латинские буквы и цифры, первый символ должен быть буквой!"
      });
      return;
    }
    
    if(!passregex.test(req.body.password)) {
      res.status(453).send({
        message: "Минимальная длина пароля: 8 символов. Ваш пароль должен содержать строчные и заглавные латинские буквы, а также цифры."
      });
      return;
    }

    if(!nameregex.test(req.body.name)) {
      res.status(454).send({
        message: "Имя пользователя должно содержать только буквы."
      });
      return;
    }
    
    // name
    User.findOne({
      where: {
        login: req.body.login
      }
    }).then(user => {
      if (user) {
        res.status(400).send({
          message: "Пользователь с данным логином уже зарегистрирован!"
        });
        return;
      }
      next();
    });
  } else {
    res.status(422).send({
      message: "Неверный формат ввода!"
    });
    return;
  }
};

const verifySignUp = {
  checkDuplicateUsernameOrEmail: checkDuplicateUsernameOrEmail
};

module.exports = verifySignUp;