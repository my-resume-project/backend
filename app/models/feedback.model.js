module.exports = (sequelize, DataTypes) => {
  const Feedback = sequelize.define("feedback", {
    email: {
      type: DataTypes.STRING
    },
    subject: {
      type: DataTypes.STRING
    },
    message: {
      type: DataTypes.STRING
    },
  }, { freezeTableName: true });

  return Feedback;
};
