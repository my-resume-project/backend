module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define("user", {
    name: {
      type: Sequelize.STRING
    },
    login: {
      type: Sequelize.STRING
    },
    token: {
      type: Sequelize.STRING
    }
  }, { freezeTableName: true });

  return User;
};